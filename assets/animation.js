document.addEventListener('DOMContentLoaded', () => {
    const yesButton = document.getElementById('yes_button');
    const noButton = document.getElementById('no_button');

    yesButton.addEventListener('click', () => {
        document.getElementById('question').style.display = 'none';
        document.getElementById('yippee').style.display = 'block';
        document.querySelector('header').style.display = 'none';
        yesButton.style.display = 'none';
        noButton.style.display = 'none';

        const colors = ["#e03776", "#8f3e98", "#4687bf", "#3bab6f", "#f9c25e", "#f47274"];
        const SVG_NS = 'http://www.w3.org/2000/svg';
        const SVG_XLINK = "http://www.w3.org/1999/xlink";

        let heartsRy = []

        function useTheHeart(n) {
            let use = document.createElementNS(SVG_NS, 'use');
            use.n = n;
            use.setAttributeNS(SVG_XLINK, 'xlink:href', '#heart');
            use.setAttributeNS(null, 'transform', `scale(${use.n})`);
            use.setAttributeNS(null, 'fill', colors[n % colors.length]);
            use.setAttributeNS(null, 'x', -69);
            use.setAttributeNS(null, 'y', -69);
            use.setAttributeNS(null, 'width', 138);
            use.setAttributeNS(null, 'height', 138);

            heartsRy.push(use)
            hearts.appendChild(use);
        }

        for (let n = 18; n >= 0; n--) { useTheHeart(n) }

        function Frame() {
            window.requestAnimationFrame(Frame);
            for (let i = 0; i < heartsRy.length; i++) {
                if (heartsRy[i].n < 18) {
                    heartsRy[i].n += .01
                } else {
                    heartsRy[i].n = 0;
                    hearts.appendChild(heartsRy[i])
                }
                heartsRy[i].setAttributeNS(null, 'transform', `scale(${heartsRy[i].n})`);
            }
        }

        Frame()

    });

    let buttonWidth = 100; // Initial width of the yesButton
    let negativeButtonWidth = 80; // Initial width of the noButton
    let noCounter = 0;

    noButton.addEventListener('click', () => {
        
        negativeButtonWidth -= 5;
        noButton.style.height = negativeButtonWidth + 'px';
        noButton.style.width = negativeButtonWidth + 'px';

        if (negativeButtonWidth <= 25) {
            noButton.style.display = 'none';
        }

        buttonWidth += 25;
        yesButton.style.height = buttonWidth + 'px';
        yesButton.style.width = buttonWidth + 'px';
        noButton.style.fontSize = (parseFloat(window.getComputedStyle(noButton).fontSize) - 8) + 'px';
    });

    noButton.addEventListener('mouseenter', (button) => {
        const randomX = Math.random() * 150 - Math.random() * 150;
        const randomY = Math.random() * 150 - Math.random() * 150;
        button.target.style.transform = `translate(${randomX}px, ${randomY}px)`;
    });
      
    noButton.addEventListener('mouseleave', (button) => {
        button.target.style.transform = 'translate(0, 0)';
    });
});


